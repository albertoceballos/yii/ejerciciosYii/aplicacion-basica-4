<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\Articulo;



class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $articulos= Articulo::find()->all();
        return $this->render('index',['articulos'=>$articulos]);
    }

    public function actionBack(){
         return $this->render('back');
    }
}
