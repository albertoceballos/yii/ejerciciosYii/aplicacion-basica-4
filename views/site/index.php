<?php

/* @var $this yii\web\View */

$this->title = 'listado';

?>
<div class="site-index">
    <ul>
        <?php
            foreach($articulos as $articulo){
                echo $this->render("index/articulo",['datos'=>$articulo]);
                $fotoArticulo=$articulo->fotos;
                foreach($fotoArticulo as $foto){
                    echo $this->render("index/_fotos",["datos"=>$foto]);
                }
            }
           /*$a= \app\models\Foto::find()->where(['articulo'=>$registro->id])->all();
           var_dump($a);*/
        ?>
    </ul>
</div>
