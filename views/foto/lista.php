<?php

use yii\helpers\Html;

?>


<div class="row">
     <?php
      foreach ($datos as $fotos){ 
     ?>
  <div class="col-xs-6 col-md-3">
    <a href="#" class="thumbnail">
      <?= Html::img('@web/imgs/'.$fotos["nombre"]); ?>  
    </a>
  </div>
    <?php
      }
    ?>
</div>